systemctl disable --now firewalld

dnf install -y nfs-utils cryptsetup iscsi-initiator-utils
systemctl enable iscsid.service --now
dnf update -y
dnf clean all

curl -sfL https://get.rke2.io | INSTALL_RKE2_TYPE=server sh -
systemctl enable --now rke2-server.service

ln -s $(find /var/lib/rancher/rke2/data/ -name kubectl) /usr/local/bin/kubectl
export KUBECONFIG=/etc/rancher/rke2/rke2.yaml
kubectl get node

curl -#L https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
helm repo add rancher-latest https://releases.rancher.com/server-charts/latest
helm repo add jetstack https://charts.jetstack.io
helm repo add longhorn https://charts.longhorn.io
helm repo update

kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.6.1/cert-manager.crds.yaml
helm upgrade -i cert-manager jetstack/cert-manager -n cert-manager --create-namespace
helm upgrade -i rancher rancher-latest/rancher --create-namespace --namespace cattle-system --set hostname=rancher.aqio.xyz --set replicas=1 --set bootstrapPassword=bootStrapAllTheThings
helm upgrade -i longhorn longhorn/longhorn --namespace longhorn-system --create-namespace

echo "Server Token"
echo $(</var/lib/rancher/rke2/server/node-token)
echo "Server IP"
ip -4 addr show eth0 | grep -oP '(?<=inet\s)\d+(\.\d+){3}'